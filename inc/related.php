<!-- カテゴリ情報を元に関連記事をランダムに呼び出す -->

<?php
$categories = get_the_category($post->ID);
$category_ID = array();
foreach($categories as $category):
array_push( $category_ID, $category -> cat_ID);
endforeach ;
$args = array(
  'post__not_in' => array($post -> ID),
  'posts_per_page'=> 4, //表示記事数
  'category__in' => $category_ID,
  'orderby' => 'rand',
);
$query = new WP_Query($args); ?>
  <?php if( $query -> have_posts() ): ?>
  <?php while ($query -> have_posts()) : $query -> the_post(); ?>
    <div class="related clearfix">

      <!-- サムネイル表示 -->
      <div class="image">
        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail(); ?></a>
      </div>

      <div class="text">

        <!-- タイトル表示 -->
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

        <!-- 記事内容抜粋 -->
        <p class="related_except"><?php echo mb_substr( strip_tags( $post->post_content  ), 0, 70 ) . '...'; ?></p>
        <p class="related_more"><a href="<?php the_permalink(); ?>">記事を読む</a></p>

      </div>

    </div>

  <?php endwhile;?>
  <?php else:?><p>NO CONTENTS</p><?php endif; wp_reset_postdata(); ?>
