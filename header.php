<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri();?>" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jquery.bxslider.css" media="screen" title="no title" charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Cuprum" rel="stylesheet">

    <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider/jquery.bxslider.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/drawer.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js" type="text/javascript"></script>
    <title>unlimited journal</title>
  </head>
  <body>
    <div class="inner drawer drawer--right">
      <header id="header" class="drawer-navbar white" role="banner">
        <div class="drawer-container">
          <div class="drawer-navbar-header">
            <a href="<?php echo home_url(); ?>">
              <h1 class="header-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" srcset="<?php echo get_template_directory_uri(); ?>/images/logo@2x.png 2x" alt="" />
              </h1>
            </a>
            <button type="button" class="drawer-toggle drawer-hamburger">
              <span class="sr-only">toggle navigation</span>
              <span class="drawer-hamburger-icon"></span>
            </button>
          </div>
          <nav class="drawer-nav" role="navigation">
            <ul class="drawer-menu drawer-menu--right">
              <?php wp_list_categories('orderby=name&title_li='); ?>
            </ul>
          </nav>
        </div>
      </header>
      <main role="main">
        <!-- Page content -->
      </main>
    </div>
