<?php get_header(); ?>

<div class="wrapper top-margin">
	<section id="slider" class="darkgray">
		<div class="inner">
			<div class="bxslider">
				<?php query_posts('showposts=15&offset=0'); if(have_posts()): while(have_posts()): the_post(); ?>
					<a href="<?php the_permalink();?>">
						<div class="slider-upper image-centered">
							<?php if(has_post_thumbnail()): ?>
								<?php the_post_thumbnail(); ?>
							<?php else: ?>
								<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />
							<?php endif; ?>
						</div>
						<div class="slider-down">
							<div class="slider-date">
								<?php echo get_post_time('d M'); ?>
							</div>
							<div class="slider-title">
								<p><?php the_title(); ?></p>
							</div>
							<div class="slider-paragraph">
								<p>abstract</p>
								<?php the_excerpt(); ?>
							</div>
						</div>
					</a>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>

	<section id="single-row" class="inner">
		<h2 class="header-title">
			<span>latest</span>
			<p>最新記事</p>
		</h2>
		<?php query_posts('showposts=1&offset=3'); if(have_posts()): while(have_posts()): the_post(); ?>
			<article class="largeModule clearfix">
				<a href="<?php the_permalink();?>">
					<div class="image image-centered black">
						<?php if(has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />
						<?php endif; ?>
					</div>
					<div class="text gray">
						<div class="square"></div>
						<div class="date">
							<?php echo get_post_time('d M'); ?>
						</div>
						<div class="title">
							<p><?php the_title(); ?></p>
							<div class="title-border"></div>
						</div>
						<div class="paragraph">
							<?php the_excerpt(); ?>
						</div>
					</div>
				</a>
			</article>
		<?php endwhile; endif; ?>
		<?php query_posts('showposts=1&offset=4'); if(have_posts()): while(have_posts()): the_post(); ?>
			<article class="largeModule clearfix">
				<a href="<?php the_permalink();?>">
					<div class="text text-reverse gray">
						<div class="square square-reverse"></div>
						<div class="date date-reverse">
							<?php echo get_post_time('d M'); ?>
						</div>
						<div class="title">
							<p><?php the_title(); ?></p>
							<div class="title-border"></div>
						</div>
						<div class="paragraph">
							<?php the_excerpt(); ?>
						</div>
					</div>
					<div class="image image-centered black">
						<?php if(has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />
						<?php endif; ?>
					</div>
				</a>
			</article>
		<?php endwhile; endif; ?>
		<?php query_posts('showposts=1&offset=5'); if(have_posts()): while(have_posts()): the_post(); ?>
			<article class="largeModule clearfix">
				<a href="<?php the_permalink();?>">
					<div class="image image-centered black">
						<?php if(has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />
						<?php endif; ?>
					</div>
					<div class="text gray">
						<div class="square"></div>
						<div class="date">
							<?php echo get_post_time('d M'); ?>
						</div>
						<div class="title">
							<p><?php the_title(); ?></p>
							<div class="title-border"></div>
						</div>
						<div class="paragraph">
							<?php the_excerpt(); ?>
						</div>
					</div>
				</a>
			</article>
		<?php endwhile; endif; ?>
	</section>

	<section id="double-row" class="gray clearfix">
		<div class="inner">
			<h2 class="header-title">
				<span>people</span>
				<p>興味のある人</p>
			</h2>
			<div class="mediumModule-inner">
				<?php query_posts('showposts=6&offset=6'); if(have_posts()): while(have_posts()): the_post(); ?>
					<article class="post-<?php the_ID();?> mediumModule">
						<a href="<?php the_permalink();?>">
							<div class="image image-centered">
								<?php if(has_post_thumbnail()): ?>
									<?php the_post_thumbnail(); ?>
								<?php else: ?>
									<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />
								<?php endif; ?>
							</div>
							<div class="text">
								<div class="title">
									<h2><span><?php the_title(); ?>。これも二行になった場合の確認。</span></h2>
									<div class="title-border"></div>
								</div>
								<div class="paragraph">
									<?php the_excerpt(); ?>
								</div>
								<div class="date black">
									<?php echo get_post_time('d M'); ?>
								</div>
							</div>
						</a>
					</article>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>

	<section id="linearContents" class="inner clearfix">
		<h2 class="header-title">
			<span>articles</span>
			<p>記事一覧</p>
		</h2>
		<?php query_posts('showposts=10&offset=0'); if(have_posts()): while(have_posts()): the_post(); ?>
			<article class="post-<?php the_ID();?> linearModule clearfix">
				<a href="<?php the_permalink();?>">
					<div class="image image-centered black">
						<div class="mask">
							<p class="theme-font"><span>READ MORE<span class="arrow">></span></span></p>
						</div>
						<?php if(has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />
						<?php endif; ?>
					</div>
					<div class="text">
						<div class="title-block">
							<h3 class="title"><?php the_title(); ?></h3>
							<div class="title-border"></div>
							<div class="date"><?php echo get_post_time('d M'); ?></div>
						</div>
						<div class="paragraph-block">
							<div class="paragraph">
								<?php the_excerpt(); ?>
							</div>
						</div>
					</div>
				</a>
			</article>
		<?php endwhile; endif; ?>
	</section>

</div>
<?php get_footer(); ?>
