(function() {
	tinymce.create('tinymce.plugins.MyButtons', {
		init : function(ed, url) {
			// imageボタン設定
			ed.addButton( 'button_image', {
				title : 'image',
				image : url + '/image.png',
				cmd: 'button_image_cmd'
			});
			// imageボタンの動作
			ed.addCommand( 'button_image_cmd', function() {
				var selected_text = ed.selection.getContent();
				var return_text = '';
				return_text = '<div class="image image-centered">' + selected_text + '</div>';
				ed.execCommand('mceInsertContent', 0, return_text);
			});
			// spanボタン設定
			ed.addButton( 'button_span', {
				title : 'span',
				image : url + '/span.png',
				cmd: 'button_span_cmd'
			});
			// spanボタンの動作
			ed.addCommand( 'button_span_cmd', function() {
				var selected_text = ed.selection.getContent();
				var return_text = '';
				return_text = '<span class="box">' + selected_text + '</span>';
				ed.execCommand('mceInsertContent', 0, return_text);
			});
			// h3ボタン設定
			ed.addButton( 'button_h3', {
				title : 'h3',
				image : url + '/h3.png',
				cmd: 'button_h3_cmd'
			});
			// h3ボタンの動作
			ed.addCommand( 'button_h3_cmd', function() {
				var selected_text = ed.selection.getContent();
				var return_text = '';
				return_text = '<h3>' + selected_text + '</h3>';
				ed.execCommand('mceInsertContent', 0, return_text);
			});
		},
		createControl : function(n, cm) {
			return null;
		},
	});
	/* Start the buttons */
	tinymce.PluginManager.add( 'custom_button_script', tinymce.plugins.MyButtons );
})();
