<?php

// ビジュアルエディターに独自のスタイルを適用
add_editor_style();

// アイキャッチ画像の有効化
add_theme_support('post-thumbnails');

// 抜粋文字数の変更
add_filter('the_excerpt', 'my_the_excerpt');
function my_the_excerpt($postContent) {
    $postContent = mb_strimwidth($postContent, 0, 300, "…","UTF-8");
    return $postContent;
}

// 管理画面　投稿ページにボタン追加
function custom_mce_buttons( $buttons ) {
  $buttons[] = 'button_image'; // <image>用
	$buttons[] = 'button_span'; // <span>用
	$buttons[] = 'button_h3'; // <h3>用
	return $buttons;
}

function custom_mce_external_plugins( $plugin_array ) {
	$plugin_array['custom_button_script'] = get_template_directory_uri() . "/tinymce.js";
	return $plugin_array;
}

add_filter( 'mce_buttons', 'custom_mce_buttons' );
add_filter( 'mce_external_plugins', 'custom_mce_external_plugins' );

?>
