<?php get_header(); ?>

<section class="single top-margin">
<?php if(have_posts()): while(have_posts()): the_post();?>
<article class="post-<?php the_ID();?>">
	<div class="wrapper black">
		<div class="inner">
			<a href="<?php the_permalink(); ?>">
				<?php if(has_post_thumbnail()): ?>
					<div class="image image-centered">
						<?php the_post_thumbnail();
					    if(get_post(get_post_thumbnail_id())->post_excerpt) {
					    echo '<a class="citation" href="' . get_post(get_post_thumbnail_id())->post_excerpt . '">出展：' . get_post(get_post_thumbnail_id())->post_excerpt . '</a>'; } ?>
				<?php else: ?>
					<div class="image image-centered">
						<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />

				<?php endif; ?>
						<div class="title">
							<h2 class="image-centered"><span><?php the_title(); ?></span></h2>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>

	<div class="wrapper gray">
		<div class="inner">
			<div class="excerpt">
				<div class="num">
					<span><?php the_ID(); ?></span>
					<span><?php the_title(); ?></span>
				</div>

				<!-- <div class="date">
					<?php the_date('Y.m.d'); ?>
				</div> -->

				<div class="desc">
					<?php the_excerpt(); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="single wrapper">
		<div class="inner">
			<div class="paragraph">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article>
</section>


<aside class="gray">
	<div class="inner">

	<?php	$nextpost=get_adjacent_post(false,'',false); if($nextpost): ?>
	<div class="next-post">
	<a href="<?php echo get_permalink($nextpost->ID); ?>">
	<?php echo get_the_post_thumbnail($nextpost->ID); ?>
	<p><?php echo esc_attr($nextpost->post_title); ?></p>
	</a>
	</div>
	<?php endif; ?>

	<?php	$prevpost=get_adjacent_post(false,'',true); if($prevpost): ?>
	<div class="prev-post">
	<a href="<?php echo get_permalink($prevpost->ID); ?>">
	<?php echo get_the_post_thumbnail($prevpost->ID); ?>
	<p><?php echo esc_attr($prevpost->post_title); ?></p>
	</a>
	</div>
	<?php endif; ?>
	</div>
</aside>

	<div class="inner">
		<?php include( TEMPLATEPATH . '/inc/related.php' ); ?>
	</div>
	<?php endwhile; endif; ?>

<?php get_footer(); ?>
