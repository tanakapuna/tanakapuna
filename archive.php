<?php get_header(); ?>

	<section id="archives" class="inner clearfix top-margin">
    <?php
      $category = get_the_category();
    	$cat_name = $category[0]->cat_name;
    	$cat_slug = $category[0]->category_nicename;
      $args = 'category_name=' . $cat_slug . '&showposts=10&offset=0';
    ?>
		<h2 class="header-title">
			<span><?php echo $cat_slug; ?></span>
			<p><?php echo $cat_name; ?></p>
		</h2>
		<?php query_posts($args); if(have_posts()): while(have_posts()): the_post(); ?>
			<article class="post-<?php the_ID();?> linearModule clearfix">
				<a href="<?php the_permalink();?>">
					<div class="image image-centered black">
						<div class="mask">
							<p class="theme-font"><span>READ MORE<span class="arrow">></span></span></p>
						</div>
						<?php if(has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" srcset="<?php echo get_template_directory_uri(); ?>/images/no-image@2x.png 2x" alt="" />
						<?php endif; ?>
					</div>
					<div class="text">
						<div class="title-block">
							<h3 class="title"><?php the_title(); ?></h3>
							<div class="title-border"></div>
							<div class="date"><?php echo get_post_time('d M'); ?></div>
						</div>
						<div class="paragraph-block">
							<div class="paragraph">
								<?php the_excerpt(); ?>
							</div>
						</div>
					</div>
				</a>
			</article>
		<?php endwhile; endif; ?>
	</section>

</div>
<script src="<?php bloginfo('template_url'); ?>/js/app.js" type="text/javascript"></script>
<?php get_footer(); ?>
